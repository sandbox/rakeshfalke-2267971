<?php

/**
 * @file
 * The node_statistics.admin.module
 */

/**
 * Form constructor for the node_statistics_article_metrics settings form.
 *
 * @see hook_admin_settings_validate()
 *
 * @ingroup forms
 */
function task_automation_content_add_task() {
  $form = array();
  $task_hr = range(0, 24);
  $task_min = range(0, 59);
  $automation_op['un_publish'] = t('un-publish');
  $automation_op['publish'] = t('Publish');
  $form['#attributes'] = array('id' => array('content-task-automation'));
  $form['content_task'] = array(
    '#type' => 'textfield',
    '#title' => t('Type title of content.'), 
    '#autocomplete_path' => 'task-automation/autocomplete',
    '#required' => TRUE,
  );
  $form['automation_date'] = array(
    '#type' => 'date',
    '#title' => t('Date'),
    '#description' => t('schedule date and time for operation to execute.'),
    '#required' => TRUE,
  );
  $form['automation_hr'] = array(
    '#type' => 'select',
    '#options' => $task_hr,
    '#title' => t('Hours'),
    '#description' => t('Hours for operation to execute.'),
    '#required' => TRUE,
  );
  $form['automation_min'] = array(
    '#type' => 'select',
    '#options' => $task_min,
    '#title' => t('Minutes'),
    '#description' => t('Minutes for operation to execute.'),
    '#required' => TRUE,
  );
  $form['automation_op'] = array(
    '#type' => 'select',
    '#options' => $automation_op,
    '#title' => t('Operation'),
    '#description' => t('Operation publishing/un-publishing.'),
    '#required' => TRUE,
  );
  $form['#validate'][] = 'task_automation_content_add_task_validate';
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  return $form;
}

/**
 * Form validation handler for hook_admin_settings().
 */
function task_automation_content_add_task_submit($form, &$form_state) {
  $task = array();
  $arg = explode(':', $form_state['values']['content_task']);
  $task['task_arg'] = $arg[1];
  $task['task_timestamp'] = _task_automation_get_timestamp($form_state['values']);
  $task['task_type'] = 'content';
  $task['task_operation'] = $form_state['values']['automation_op'];
  if ($task_id = _task_automation_content_task_insert($task)) {
    drupal_set_message(t('Task submitted.Generated task id @task', array('@task' => $task_id)));
  }
}

function task_automation_content_add_task_validate($form, &$form_state) {
  $arg = explode(':', $form_state['values']['content_task']);
  $task_timestamp = _task_automation_get_timestamp($form_state['values']);
  $current_time = time();
  $task_date = new DateTime(date('Y-m-d H:m', $task_timestamp));
  $current_date = new DateTime(date('Y-m-d H:m', $current_time));
  $interval = $current_date->diff($task_date);
  $task_details['task_timestamp'] = $task_timestamp;
  $task_details['task_arg'] = $arg[1];
  $task_details['task_operation'] = $form_state['values']['automation_op'];
  if ($interval->invert) {
    form_set_error($form_state['values']['automation_op'], t('Past date not supported!'));
  }
  $row_count = _task_automation_content_task_exist($task_details);
  if ($row_count) {
    form_set_error($form_state['values']['automation_op'], t('Task exist for content in system!'));
  }
}

function _task_automation_content_task_insert($task) {
  global $user;
  $task_id = db_insert('task_automation')->fields(array(
    'task_type' => $task['task_type'],
    'task_timestamp' => $task['task_timestamp'],
    'task_arg' => $task['task_arg'],
    'task_uid' => $user->uid,
    'task_operation' => $task['task_operation'],
    'timestamp' => REQUEST_TIME,
  ))->execute();
  return $task_id;
}

function _task_automation_content_task_exist($task_details) {
  $data_count = db_select('task_automation', 'ta')
    ->fields('ta', array('task_id'))
    ->condition('task_timestamp', $task_details['task_timestamp'])
    ->condition('task_arg', $task_details['task_arg'])
    ->condition('task_operation', $task_details['task_operation'])
    ->execute()->rowCount();
  return $data_count;
}

function _task_automation_content_task_count() {
  $data_count = db_select('task_automation', 'ta')
    ->fields('ta', array('task_id'))
    ->execute()->rowCount();
  return $data_count;
}

function _task_automation_content_task_delete($task_id) {
  $data_count = db_select('task_automation', 'ta')
    ->fields('ta', array())
    ->condition('task_id', $task_id)
    ->execute()->fetchAll();
  $task_log_id = db_insert('task_automation_log')->fields(array(
    'task_id' => $task_id,
    'task_data' => drupal_json_encode($data_count[0]),
    'timestamp' => REQUEST_TIME,
  ))->execute();  
  $num_deleted = db_delete('task_automation')
    ->condition('task_id', $task_id)
    ->execute();
  return $num_deleted;
}

function _task_automation_content_task_execute() {
  $task = _task_automation_content_task_count();
  while ($task) {
  //current timestamp
  $current_time = time();
  $task_list = db_select('task_automation', 'ta')
    ->fields('ta', array('task_id', 'task_timestamp', 'task_operation', 'task_arg'))
    ->condition('task_timestamp', $current_time)
    ->orderBy('task_timestamp', 'ASC')
    ->execute()->fetchAll();
    if (count($task_list)) {
      foreach ($task_list as $task) {
        $task_arg = $task->task_arg;
        $function_name = '_task_automation_content_';
        $function_name .= 'op_' . $task->task_operation;
        $function_name($task_arg);//
        _task_automation_content_task_delete($task->task_id);
        $task = _task_automation_content_task_count();
      }
    }
  }
  return TRUE;
}

function _task_automation_content_op_publish($node_id) {
  $node = node_load($node_id);
  $node->status = 1;
  node_save($node);
  return TRUE;
}

function _task_automation_content_op_un_publish($node_id) {
  $node = node_load($node_id);
  $node->status = 0;
  node_save($node);
  return TRUE;
} 